#include <stdio.h>                                                                                                  
#include <stdint.h>                                                                                                 
#include <stdlib.h>                                                                                                 
#include <time.h>                                                                                                   
                                                                                                                    
static uint16_t                                                                                                     
prim(uint16_t a, uint16_t b)                                                                                        
{                                                                                                                   
        static const uint_fast16_t c[] = { 1, 0 };                                                                  
        a = a + c[a & 1];                                                                                           
        b = b - c[b & 1];                                                                                           
                                                                                                                    
        for (; a <= b; ++a) {                                                                                       
                for (uint16_t i = 3; i < a / 2; ++i) {                                                              
                        if (a % i == 0)                                                                             
                                goto _PRIM_CONT_;                                                                   
                }                                                                                                   
                                                                                                                    
                return a;                                                                                           
_PRIM_CONT_:                                                                                                        
                continue;                                                                                           
        }                                                                                                           
                                                                                                                    
        return 0;                                                                                                   
}                                                                                                                   
                                                                                                                    
static uint16_t                              
sqml(uint16_t x, uint16_t k, uint16_t p)     
{                                            
        int c;                               
        c  = __builtin_clz(k);               
        c -= 16;                             
                                             
        k <<= c + 1;                         
                                             
        uint_fast32_t r;                     
        r = x;                               
        for (int i = c + 1; i < 16; ++i) {   
                r = (r * r) % p;             
                if (k & 0x8000)              
                        r = (r * x) % p;     
                k <<= 1;                     
        }                                    
                                             
        return r;                            
}                                                                                         
                                                 
static uint16_t                                 
gen(uint16_t p)                                 
{                                               
        // Z_p* = { 1, 2, ..., p - 1 }          
        // g => |Z_p*| = len(g)                 
                                                
        for (uint16_t i = 2, j; i < p; ++i) {   
                for (j = 2; j < p; ++j)         
                        if (sqml(i, j, p) == 1) 
                                break;          
                                                
                if (j == p - 1)                 
                        return i;               
        }                                       
                                                
        return 0;                               
}                                                                                        
                                             
static uint16_t                              
pick(uint16_t a, uint16_t b)                 
{                                            
        uint16_t r;                          
        // r in [a, b]                       
        r = rand() % (b + 1 - a) + a;        
        return r;                            
}                                            
                                             
int                                          
main(void)                                   
{                                            
        uint16_t p;                          
        p = prim(10, 20);                    
        printf("p: %u\n", p);                
                                             
        uint16_t g;                          
        g = gen(p);                          
        printf("g: %u\n", g);                
    
        srand(time(NULL));
                                                               
        uint16_t a;                                   
        a = pick(1, p - 2);                           
        printf("a: %u\n", a);                         
                                                    
        uint16_t A;                                   
        A = sqml(g, a, p);                            
        printf("A: %u\n", A);                         
                                                    
        uint16_t b;                                   
        b = pick(1, p - 2);                           
        printf("b: %u\n", b);                         
                                                    
        uint16_t B;                                   
        B = sqml(g, b, p);                            
        printf("B: %u\n", B);                         
                                                    
        uint16_t K_AB, K_BA;                          
        K_AB = sqml(A, b, p);                         
        K_BA = sqml(B, a, p);                         
        printf("K_AB (%u) = K_BA (%u)\n", K_AB, K_BA);
                                                    
        return 0;                                     
}                                                     
